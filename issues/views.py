import uuid
from collections import defaultdict
from datetime import datetime

from django.conf import settings
from django.contrib.auth.decorators import user_passes_test, login_required
from django.db.models import Q
from django.http import HttpResponse, JsonResponse, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse

from accounts.models import is_user_customer, is_user_developer
from accounts.moderated import moderation_required
from customers.models import Developer
from issues import mailer
from issues.forms import IssueForm, CommentForm
from issues.models import Issue, Block, Item, Comment, IssueStatus, File
from projects.models import Project, ProjectDeveloper


@login_required
@moderation_required
def issue_list(request):
    if is_user_developer(request.user):
        return developer_issue_list(request)
    if is_user_customer(request.user):
        return customer_issue_list(request)


@user_passes_test(is_user_customer)
def customer_issue_list(request):

    issues_qs = Issue.objects.select_related('project').filter(author=request.user.profile)
    issues_not_done_qs = issues_qs.exclude(status=IssueStatus.done)
    issues_done_qs = issues_qs.filter(status=IssueStatus.done)

    issues = defaultdict(list)
    projects_qs = Project.objects.filter(customer=request.user.profile.customer)
    for p in projects_qs:
        issues[p] = []

    for qs in [issues_not_done_qs, issues_done_qs]:
        for iss in qs:
            if request.user == iss.author.user:
                iss.has_comments = iss.has_unread_comments_from_developer()
            issues[iss.project].append(iss)

    issues_dict = list(issues.items())
    issues_dict.sort(key=lambda i: i[0].id)

    context = {
        'issues_dict': issues_dict,
        'profile': request.user.profile,
        'customer': request.user.profile.customer
    }

    return render(request, 'issue_list.html', context)


@user_passes_test(is_user_developer)
def developer_issue_list(request):

    profile = request.user.profile
    key_projects = ProjectDeveloper.objects.filter(user=profile,key=True).\
        values_list('project', flat=True)

    issues_qs = Issue.objects.select_related('project').filter(
        Q(developer=profile) | Q(project__id__in=key_projects)
    ).distinct()

    issues_not_done_qs = issues_qs.exclude(status=IssueStatus.done)
    issues_done_qs = issues_qs.filter(status=IssueStatus.done)

    issues = defaultdict(list)
    for qs in [issues_not_done_qs, issues_done_qs]:
        for iss in qs:
            if iss.developer and request.user == iss.developer.user:
                iss.has_comments = iss.has_unread_comments_from_customer()
            issues[iss.project].append(iss)

    issues_dict = list(issues.items())
    issues_dict.sort(key=lambda i: i[0].id)

    context = {
        'issues_dict': issues_dict,
        # 'issues_for_projects_dict': issues,
        'profile': profile,
        'customer': profile.customer
    }

    return render(request, 'issue_list.html', context)


@login_required
@moderation_required
def issue(request, id):
    try:
        id = int(id)
    except:
        raise Http404
    if is_user_developer(request.user):
        return developer_issue(request, id)
    if is_user_customer(request.user):
        return customer_issue(request, id)


@user_passes_test(is_user_customer)
def customer_issue(request, id):
    issue = get_object_or_404(Issue, id=id)
    if request.user != issue.author.user:
        raise Http404
    context = {
        'role': 'customer',
        'iss': issue,
        'profile': request.user.profile,
        'customer': request.user.profile.customer,
        'add_issue_url': reverse('issues:add_issue', args=(issue.project_id,)),
    }

    if request.user == issue.author.user:
        issue.mark_comments_from_developer_read()

    return render(request, 'issue_customer.html', context)


@user_passes_test(is_user_developer)
def developer_issue(request, id):
    issue = get_object_or_404(Issue, id=id)
    profile = request.user.profile
    if not request.user in issue.participants_users():
        raise Http404
    context = {
        'role': 'developer',
        'iss': issue,
        'profile': profile,
        'customer': issue.project.customer,
        'correction_coefficient': settings.CORRECTIONS_COEFFICIENT,
    }
    if profile in issue.project.responsible_developers():
        context['add_issue_url'] = reverse('issues:add_issue',
                                           args=(issue.project_id,))

    if issue.developer and request.user == issue.developer.user:
        issue.mark_comments_from_customer_read()
    return render(request, 'issue_developer.html', context)


@login_required
@moderation_required
def add_issue(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    profile = request.user.profile

    if profile.customer != project.customer and not profile in project.responsible_developers():
        raise Http404

    try:
        developer = ProjectDeveloper.objects.filter(project=project, key=True).first().user
    except AttributeError:
        try:
            developer = Developer.objects.filter(customer=project.customer, key=True).first().user
        except:
            developer = None


    form = IssueForm(request.POST or None)

    if form.is_valid():

        author = profile
        if author.type == author.TYPE_DEVELOPER:
            author = project.default_author() or author

        issue = Issue.objects.create(
            project=project,
            title=form.cleaned_data['title'],
            description=form.cleaned_data['description'],
            author=author,
            developer=developer
        )

        for file_id in request.POST.getlist('fileIds[]'):
            if file_id:
                try:
                    file = File.objects.get(issue=None, id=file_id, author=profile)
                    file.issue = issue
                    file.save()
                except File.DoesNotExist:
                    pass

        mailer.add_issue(issue, request.get_host(), profile)

        return redirect('issues:issues')

    context = {
        'project': project,
        'form': form,
        'profile': profile,
        'customer': profile.customer,
        'add_issue_url': reverse('issues:add_issue', args=(project.id,)),
    }

    return render(request, 'add_issue.html', context)


@user_passes_test(is_user_developer)
@moderation_required
def issue_block_developer(request, issue_id, block_id):

    issue = get_object_or_404(Issue, id=issue_id, developer=request.user.profile)

    if block_id == '0':
        block = Block(issue=issue)
    else:
        block = get_object_or_404(Block, id=block_id, issue=issue)

    if request.POST:
        block.title = request.POST.get('title', '') or 'Доработка сайта'

        if not request.POST.get('cancel-editing'):
            block.description = request.POST.get('description', '') or ''
            block.save()

            for i in block.items.all():
                if request.POST.get('item-delete-{}'.format(i.id), 0) == '1':
                    i.delete()
                else:
                    i.title = request.POST.get('item-title-{}'.format(i.id), '')
                    try:
                        i.estimate = float(request.POST.get('item-estimate-{}'.format(i.id)).replace(',', '.'))
                    except ValueError:
                        i.estimate = 0
                    i.save()

            new_items_count = int(request.POST.get('new-items-count', 0))
            if new_items_count:
                try:
                    order = Item.objects.filter(block=block).latest('order').order + 1
                except Item.DoesNotExist:
                    order = 1

                for k in range(new_items_count):

                    if request.POST.get('new-item-delete-{}'.format(k), 0) == '1':
                        continue

                    i = Item(block=block, order=order+k)
                    i.title = request.POST.get('new-item-title-{}'.format(k), None)
                    try:
                        i.estimate = float(request.POST.get('new-item-estimate-{}'.format(k), '0.0').replace(',', '.'))
                    except ValueError:
                        i.estimate = 0

                    if i.title:
                        i.save()

    context = {
        'b': block,
        'iss': issue,
        'new_block': block_id == '0'
    }

    return render(request, 'issue_block_developer.html', context)


@user_passes_test(is_user_developer)
@moderation_required
def delete_estimate_block(request, issue_id, block_id):

    issue = get_object_or_404(Issue, id=issue_id, developer=request.user.profile)
    block = get_object_or_404(Block, id=block_id, issue=issue)

    block.delete()
    return HttpResponse('')


@user_passes_test(is_user_developer)
@moderation_required
def new_item_html(request):

    current_count = int(request.GET.get('current-count', 0))
    count = int(request.GET.get('count', 1))
    items = []
    for k in range(count):
        items.append(Item(id=current_count+k, estimate=''))

    block_id = request.GET.get('block-id')
    if block_id == '0':
        block = Block(id='0')
    else:
        block = get_object_or_404(Block, id=block_id)

    context = {
        'items': items,
        'b': block
    }
    return render(request, 'item_tpl.html', context)


@user_passes_test(is_user_developer)
@moderation_required
def new_block_html(request, issue_id):

    issue = get_object_or_404(Issue, id=issue_id, developer=request.user.profile)

    context = {
        'iss': issue,
        'uuid': uuid.uuid1()
    }
    return render(request, 'block_tpl.html', context)


@login_required
@moderation_required
def files(request, issue_id):
    issue = get_object_or_404(Issue, id=issue_id)
    context = {
        'iss': issue
    }
    return render(request, 'files.html', context)


@login_required
@moderation_required
def comments(request, issue_id):

    issue = get_object_or_404(Issue, id=issue_id)
    if request.user not in issue.participants_users():
        raise Http404

    comment = Comment(issue=issue, author=request.user.profile)

    if request.GET.get('order'):
        order = request.session.get('issue_comments_order', 'create_dt')
        if order == 'create_dt':
            order = '-create_dt'
        else:
            order = 'create_dt'
        request.session['issue_comments_order'] = order
        request.session.save()

    comments = Comment.objects.filter(issue=issue)
    order = request.session.get('issue_comments_order', 'create_dt')
    if order in ['create_dt', '-create_dt']:
        comments = comments.order_by(order)

    comments = comments.order_by('-create_dt')

    context = {
        'iss': issue,
        'comments': comments,
        'order': order
    }

    form = CommentForm(request.POST or None, instance=comment)

    file_ids = request.POST.getlist('fileIds[]')
    if form.is_valid():

        comment = form.save()
        File.objects.filter(issue=issue, id__in=file_ids).update(comment=comment)

        # при написании коммента заказчиком определенные статусы меняются:
        # "Ожидание проверки" --> "Исправление замечаний"
        # "Закрыто" --> "Исправление замечаний"
        change_status_map = {
            IssueStatus.wait_work_approve: IssueStatus.wait_fix_corrections,
            IssueStatus.done: IssueStatus.wait_fix_corrections
        }

        if request.user == issue.author.user and \
                issue.status in change_status_map.keys():

            previous_status = issue.get_status_display()
            issue.status = change_status_map[issue.status] # меняем статус
            issue.save()

            comment.change_status_comment_text = \
                'Статус изменен с "{}" на "{}"'.format(
                    previous_status,
                    issue.get_status_display())
            comment.change_status_comment = True
            comment.save()

            mailer.add_issue_comment(comment, request.get_host(),
                                     request.user.profile, previous_status)
            return JsonResponse({'result': 'refresh'})
        else:
            mailer.add_issue_comment(comment, request.get_host(),
                                     request.user.profile)

        form = CommentForm()
        context['message_added'] = True
    else:
        if file_ids:
            comment = Comment.objects.create(
                issue=issue,
                author=request.user.profile,
                text='Загружены файлы')
            File.objects.filter(issue=issue, id__in=file_ids).update(
                comment=comment)

    context['form'] = form
    context['role'] = 'developer' if is_user_developer(request.user) else 'customer'

    return render(request, 'comments.html', context)


class SetStatusException(Exception):
    pass


@login_required
@moderation_required
def change_status(request, issue_id, revision, status):
    issue = get_object_or_404(Issue, id=issue_id)

    if request.user not in issue.participants_users():
        raise Http404

    try:

        previous_status = issue.get_status_display()

        if status == 'approve-estimate':
            if not is_user_customer(request.user):
                raise Http404
            if issue.status == IssueStatus.new_wait_estimate:
                if issue.estimate_revision != int(revision):
                    raise SetStatusException('Разработчик вносит измения в оценку. Пожалуйста, подождите. Вам придет '
                                             'оповещение на почту, когда оценка будет откорректирвана.')
                raise SetStatusException('Задача еще находится на оценке')
            if issue.status != IssueStatus.wait_estimate_approve:
                raise SetStatusException('Оценка уже утверждена')
            if issue.estimate_revision != int(revision):
                raise SetStatusException('Оценка задачи изменилась, пожалуйста проверьте отценку еще раз!')

            issue.status = IssueStatus.in_process
            issue.date_approve_estimate = datetime.now()

        if status == 'approve-work':
            if not is_user_customer(request.user):
                raise Http404
            if issue.status not in (IssueStatus.wait_work_approve,
                                    IssueStatus.wait_fix_corrections):
                raise SetStatusException('Задача еще в работе')
            issue.status = IssueStatus.done
            issue.date_done = datetime.now()
            if not issue.date_done_first_time:
                issue.date_done_first_time = datetime.now()

        if status == 'send-estimate':
            if not is_user_developer(request.user):
                raise Http404
            if issue.status == IssueStatus.in_process:
                raise SetStatusException('Предыдущая оценка уже утверждена')
            if issue.estimate() == 0:
                issue.status = IssueStatus.in_process
            else:
                issue.status = IssueStatus.wait_estimate_approve

        if status == 'correct-estimate':
            if not is_user_developer(request.user):
                raise Http404
            if issue.status == IssueStatus.in_process:
                raise SetStatusException('Предыдущая оценка уже утверждена')

            issue.estimate_revision += 1  # ревизия оценки увеличивается, когда разработчик нажимает "исправить оценку"
            issue.status = IssueStatus.new_wait_estimate

        if status == 'done':
            if not is_user_developer(request.user):
                raise Http404
            if issue.status not in (IssueStatus.in_process, IssueStatus.wait_fix_corrections):
                raise SetStatusException('Не правильный статус')

            issue.status = IssueStatus.wait_work_approve
            if not issue.date_ready:
                issue.date_ready = datetime.now()

        if status == 'fix-corrections':
            if request.user == issue.author.user and issue.status == IssueStatus.wait_work_approve:
                issue.status = IssueStatus.wait_fix_corrections
            else:
                return JsonResponse({'result': 'nothing'})

        mailer.change_issie_status(issue, request.user.profile, request.get_host(), previous_status)
        issue.previous_developer_estimate = issue.developer_estimate()
        issue.previous_customer_estimate = issue.customer_estimate()
        issue.save()

        Comment.objects.create(
            issue=issue, author=request.user.profile, text='',
            change_status_comment_text='Статус изменен с "{}" на "{}"'.format(
                previous_status,
                issue.get_status_display()),
            change_status_comment=True)

    except SetStatusException as e:
        return JsonResponse({'result': 'alert', 'text': str(e)})

    return JsonResponse({'result': 'refresh'})


@login_required
@moderation_required
def delete_file(request, issue_id, file_id):
    if issue_id != "None":
        issue = get_object_or_404(Issue, id=issue_id)
        if request.user not in issue.participants_users():
            raise Http404
    else:
        issue = None
    file = get_object_or_404(File, issue=issue, id=file_id, author=request.user.profile)
    file.delete()

    return JsonResponse({'result': 'deleted'})


@login_required
@moderation_required
def upload_file(request, issue_id, project_id):
    if issue_id != '0':
        issue = get_object_or_404(Issue, id=issue_id)
        if request.user not in issue.participants_users():
            raise Http404
    else:
        issue = None

    files = request.FILES.getlist('files[]')

    result = []

    for file in files:
        f = File(issue=issue, title=file.name, file=file, author=request.user.profile)
        f.save()
        result.append(f)

    return render(request, 'attached_files.html', {'files': result})


@login_required
@moderation_required
def set_issue_deadline(request, issue_id):
    issue = get_object_or_404(Issue, id=issue_id)
    if issue.developer != request.user.profile:
        raise Http404

    if issue.deadline:
        raise Http404

    issue.deadline = datetime.strptime(request.GET.get('date'), '%d.%m.%Y')
    issue.save()

    return JsonResponse({'result': 'refresh'})
