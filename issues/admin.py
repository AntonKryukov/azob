from django.contrib import admin
from django.utils.translation import gettext as _

from issues.models import Issue, Block, Item, File, Comment
import nested_admin


class ItemInline(nested_admin.NestedTabularInline):
    model = Item
    sortable_field_name = "order"
    fields = 'title', 'estimate', 'developer_estimate', 'customer_estimate', 'order', 'comment'
    readonly_fields = 'developer_estimate', 'customer_estimate'
    extra = 0


class BlockInline(nested_admin.NestedTabularInline):
    model = Block
    sortable_field_name = "order"
    inlines = [ItemInline]
    extra = 0


class FileInline(nested_admin.NestedTabularInline):
    model = File
    fields = 'file', 'title'
    extra = 0


class CommentInline(nested_admin.NestedTabularInline):
    model = Comment
    fields = 'text', 'author', 'show_for_customer', 'read', \
             'change_status_comment'
    extra = 0


@admin.register(Issue)
class IssueAdmin(nested_admin.NestedModelAdmin):
    save_on_top = True
    list_display = '__str__', 'title', 'parent', 'project', 'estimate', \
                   'developer_estimate', 'customer_estimate'
    readonly_fields = 'estimate', 'developer_estimate', 'customer_estimate', \
                      'previous_developer_estimate', \
                      'previous_customer_estimate', 'date_approve_estimate', \
                      'date_ready', 'date_done', 'date_done_first_time'

    fieldsets = (
        (None, {
            'fields': ('project', 'parent', 'title', 'description',
                       'redmine_helpdesk_number', 'developer_redmine_number',
                       'deadline', 'author', 'developer',
                       'status', 'urgency')
        }),
        (_('Оценка'), {
            'fields': (('estimate', 'developer_estimate', 'customer_estimate'),
                       'estimate_revision',
                       ('previous_developer_estimate',
                        'previous_customer_estimate')),
        }),
        (_('Даты'), {
            'fields': (('date_approve_estimate', 'date_ready', 'date_done',
                        'date_done_first_time'),),
        }),
    )

    inlines = [BlockInline, FileInline, CommentInline]


# @admin.register(File)
# class FileAdmin(admin.ModelAdmin):
#     list_display = 'file', 'issue'



