# from django import forms
from django.forms import ModelForm, formset_factory, CharField, TextInput, Textarea
# from tinymce.widgets import TinyMCE

from issues.models import Issue, File, Comment


class FileForm(ModelForm):
    class Meta:
        model = File
        fields = ['file']


FileFormSet = formset_factory(FileForm, extra=20)


class IssueForm(ModelForm):
    # description = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 7}))
    class Meta:
        model = Issue
        fields = ['title', 'description']


class EditorFieldWidget(Textarea):
    template_name = 'forms/widgets/editorfield.html'


class CommentForm(ModelForm):
    # text = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 6}))

    class Meta:
        model = Comment
        fields = ['text']
        widgets = {
            'text': EditorFieldWidget(attrs={'rows': 7, 'cols': False, 'placeholder': False})
        }
        labels = {
            'text': False
        }

