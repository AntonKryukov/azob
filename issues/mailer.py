from django.conf import settings
from django.template.loader import render_to_string
from mailer import send_html_mail


def filter_recipients(recipients, profile):
    """
    фильтруем получателей. Если у пользователя стоит галочка 'Не извещать
    об изменениях, которые я сделал сам', то такому пользователю письмо не
    шлем
    :param recipients: список кортежей (email, оценка, которую показываем)
    :param profile: профиль пользвателя, который активировал рассылку
    :return: отфильтрованный список recipients
    """

    if profile.do_not_notice_about_my_actions:
        recipients = [r for r in recipients if r[0] != profile.user.email]
    return recipients


def add_issue(issue, host, profile):
    context = {'issue': issue, 'host': host}
    topic = '[{} - Задача #{}] {}.'.format(issue.project.name, issue.id, issue.title)

    recipients = [
        (issue.author.user.email, issue.customer_estimate())
    ]
    if issue.developer:
        recipients.append((issue.developer.user.email,
                           issue.developer_estimate()))

    for key_dev in issue.key_project_developers():
        if key_dev != issue.developer:
            recipients.append((key_dev.user.email, issue.developer_estimate()))

    recipients = filter_recipients(recipients, profile)

    for email, estimate in recipients:
        context['estimate'] = estimate
        top = '{} Оценка = {} чч'.format(topic, estimate) if estimate else topic
        body_txt = render_to_string('mailer/new_issue_created.txt', context)
        body_htm = render_to_string('mailer/new_issue_created.html', context)
        send_html_mail(top, body_txt, body_htm, settings.DEFAULT_FROM_EMAIL, [email])


def change_issie_status(issue, profile, host, previous_status):
    context = {
        'issue': issue,
        'host': host,
        'profile': profile,
        'previous_status': previous_status
    }
    topic = '[{} - Задача #{}] {}.'.format(issue.project.name, issue.id, issue.title)

    recipients = [(issue.author.user.email,
                   issue.customer_estimate(),
                   issue.previous_customer_estimate)]

    if issue.developer:
        recipients.append((issue.developer.user.email,
                           issue.developer_estimate(),
                           issue.previous_developer_estimate))

    for key_dev in issue.key_project_developers():
        if key_dev != issue.developer:
            recipients.append((key_dev.user.email,
                               issue.developer_estimate(),
                               issue.previous_developer_estimate))

    recipients = filter_recipients(recipients, profile)

    for email, estimate, previous_estimate in recipients:
        context['estimate'] = estimate
        context['previous_estimate'] = previous_estimate
        top = '{} Оценка = {} чч'.format(topic, estimate) if estimate else topic
        body_txt = render_to_string('mailer/change_issue_status.txt', context)
        body_htm = render_to_string('mailer/change_issue_status.html', context)
        send_html_mail(top, body_txt, body_htm, settings.DEFAULT_FROM_EMAIL, [email])


def add_issue_comment(comment, host, profile, previous_status=None):
    issue = comment.issue
    context = {
        'comment': comment,
        'issue': issue,
        'host': host,
        'previous_status': previous_status
    }
    topic = '[{} - Задача #{}] {}.'.format(issue.project.name, issue.id, issue.title)

    recipients = [
        (issue.author.user.email, issue.customer_estimate())
    ]
    if issue.developer:
        recipients.append((issue.developer.user.email,
                           issue.developer_estimate()))

    for key_dev in issue.key_project_developers():
        if key_dev != issue.developer:
            recipients.append((key_dev.user.email, issue.developer_estimate()))

    recipients = filter_recipients(recipients, profile)

    for email, estimate in recipients:
        context['estimate'] = estimate
        top = '{} Оценка = {} чч'.format(topic, estimate) if estimate else topic
        body_txt = render_to_string('mailer/add_issue_comment.txt', context)
        body_htm = render_to_string('mailer/add_issue_comment.html', context)
        send_html_mail(top, body_txt, body_htm, settings.DEFAULT_FROM_EMAIL, [email])
