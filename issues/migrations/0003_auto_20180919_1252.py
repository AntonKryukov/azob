# Generated by Django 2.1.1 on 2018-09-19 12:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('issues', '0002_auto_20180918_1017'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='block',
            options={'ordering': ('order',), 'verbose_name': 'Блок', 'verbose_name_plural': 'Блоки'},
        ),
        migrations.AlterModelOptions(
            name='item',
            options={'ordering': ('order',), 'verbose_name': 'Пункт оценки', 'verbose_name_plural': 'Пункты оценки'},
        ),
        migrations.AlterField(
            model_name='block',
            name='issue',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='blocks', to='issues.Issue', verbose_name='Задача'),
        ),
        migrations.AlterField(
            model_name='issue',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='issues', to='projects.Project', verbose_name='Проект'),
        ),
        migrations.AlterField(
            model_name='item',
            name='block',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='issues.Block', verbose_name='Блок'),
        ),
    ]
