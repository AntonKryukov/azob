from django.urls import path

from . import views

app_name = 'issues'
urlpatterns = [
    path('', views.issue_list, name="issues"),
    # path('404/', views.p404, name="p404"),
    path('new-item/', views.new_item_html, name='issue_add_item'),
    path('new-block/<issue_id>/', views.new_block_html, name='issue_add_block'),
    path('comments/<issue_id>/', views.comments, name='comments'),
    path('files/<issue_id>/', views.files, name='files'),
    path('set-deadline/<issue_id>/', views.set_issue_deadline, name='set-deadline'),
    path('<project_id>/add/', views.add_issue, name='add_issue'),
    path('block/<issue_id>/<block_id>/', views.issue_block_developer, name='issue_block_developer'),
    path('block/<issue_id>/<block_id>/delete/', views.delete_estimate_block, name='delete-estimate-block'),
    path('status/<issue_id>/<revision>/<status>/', views.change_status, name='change-issue-status'),
    path('upload/<issue_id>/<project_id>/', views.upload_file, name='upload-file'),
    path('delete-file/<issue_id>/<file_id>/', views.delete_file, name='delete-file'),
    path('<id>/', views.issue, name='issue'),
]