import os
from datetime import datetime

from django.conf import settings
from django.db import models
from django.db.models import Manager
from django.utils.translation import gettext as _

from accounts.models import Profile
from projects.models import Project, ProjectDeveloper
import markdown


class IssueStatus(object):
    new_wait_estimate = 10
    wait_estimate_approve = 20
    in_process = 30
    wait_work_approve = 50
    wait_fix_corrections = 55
    done = 60

    CHOICES = (
        (new_wait_estimate, _('На оценку')),
        (wait_estimate_approve, _('Подтвердить оценку')),
        (in_process, _('В работе')),
        (wait_work_approve, _('Ожидание проверки')),
        (wait_fix_corrections, _('Исправление замечаний')),
        (done, _('Закрыто'))
    )


class IssueUrgency(object):
    usual = 10
    urgent = 20
    very = 30

    CHOICES = (
        (usual, _('Обычная')),
        (urgent, _('Срочная')),
        (very, _('Очень'))
    )


class Issue(models.Model):
    project = models.ForeignKey(Project, verbose_name=_("Проект"), related_name='issues', on_delete=models.CASCADE)
    parent = models.ForeignKey('self', verbose_name=_("Родительская задача"), null=True, blank=True,
                               on_delete=models.CASCADE)
    title = models.CharField(_("Заголовок"), max_length=250)
    description = models.TextField(_("Описание"), null=True, blank=True)
    # description = HTMLField(_("Описание"), null=True, blank=True)
    redmine_helpdesk_number = models.IntegerField(_("Номер в Redmine HelpDESK"), null=True, blank=True)
    developer_redmine_number = models.IntegerField(_("Номер в Redmine Разработчика"), null=True, blank=True)
    create_dt = models.DateTimeField(_("Дата создания"), auto_now_add=True)
    deadline = models.DateField(_("Дата дедлайна"), null=True, blank=True)
    author = models.ForeignKey(Profile, verbose_name=_("Автор задачи"), on_delete=models.CASCADE,
                               related_name='author_for')
    developer = models.ForeignKey(Profile, verbose_name=_("Исполнитель задачи"), on_delete=models.CASCADE, null=True,
                                  blank=True, related_name='developer_for')
    status = models.IntegerField("Статус задачи", choices=IssueStatus.CHOICES, default=IssueStatus.new_wait_estimate)
    urgency = models.IntegerField("Срочность задачи", choices=IssueUrgency.CHOICES, default=IssueUrgency.usual)
    estimate_revision = models.PositiveIntegerField("Ревизия оценки", default=0)

    previous_developer_estimate = models.FloatField(_("Прошлая оценка разработчика"), null=True, blank=True)
    previous_customer_estimate = models.FloatField(_("Прошлая оценка заказчика"), null=True, blank=True)

    date_approve_estimate = models.DateTimeField(_("Дата отправки в работу"),
                                                 null=True, blank=True)
    date_ready = models.DateTimeField(_("Дата изменения статуса на «готово»"),
                                      null=True, blank=True)
    date_done = models.DateTimeField(_("Дата изменения статуса на «закрыт»"),
                                     null=True, blank=True)
    date_done_first_time = models.DateTimeField(
        _("Дата изменения статуса на «закрыт» в первый раз"),
        null=True, blank=True)

    objects = Manager()

    class Meta:
        verbose_name = _("Задача")
        verbose_name_plural = _("Задачи")
        ordering = '-create_dt',

    def __str__(self):
        return "#{} {} [{}]".format(self.pk, self.title, self.create_dt.strftime(settings.DATETIME_FORMAT))

    def html_description(self):
        return markdown.markdown(self.description, extensions=['nl2br'])

    def is_deadline(self):
        return self.deadline < datetime.today().date()

    def estimate(self):
        return round(sum([i.estimate for i in Item.objects.filter(block__issue=self)]), 2)
    estimate.short_description = _("Оценка")

    def developer_estimate(self):
        return round(sum([i.developer_estimate() for i in Item.objects.filter(block__issue=self)]), 2)
    developer_estimate.short_description = _("+50%")

    def customer_estimate(self):
        return round(sum([i.customer_estimate() for i in Item.objects.filter(block__issue=self)]), 2)
    customer_estimate.short_description = _("Оценка для заказчика")

    def is_status_wait_estimate(self):
        return self.status == IssueStatus.new_wait_estimate

    def is_status_wait_estimate_approve(self):
        return self.status == IssueStatus.wait_estimate_approve

    def is_status_in_process(self):
        return self.status == IssueStatus.in_process

    def is_status_wait_work_approve(self):
        return self.status == IssueStatus.wait_work_approve

    def is_status_wait_fix_corrections(self):
        return self.status == IssueStatus.wait_fix_corrections

    def is_status_done(self):
        return self.status == IssueStatus.done

    def has_unread_comments_from_customer(self):
        return self.comments.filter(author=self.author, read=False).count() > 0

    def has_unread_comments_from_developer(self):
        return self.comments.filter(author=self.developer, read=False).count() > 0

    def mark_comments_from_customer_read(self):
        # помечаем, что все сообщения от заказчика в этой задаче - прочитаны
        self.comments.filter(author=self.author, read=False).update(read=True)

    def mark_comments_from_developer_read(self):
        # помечаем, что все сообщения от разработчика в этой задаче - прочитаны
        self.comments.filter(author=self.developer, read=False).update(read=True)

    def participants_users(self):
        participants = [self.author.user]

        if self.developer:
            participants.append(self.developer.user)

        for project_key_developer in ProjectDeveloper.objects.filter(
                project=self.project, key=True):

            if self.developer != project_key_developer:
                participants.append(project_key_developer.user.user)

        return participants

    def key_project_developers(self):
        return [pd.user for pd in
                ProjectDeveloper.objects.filter(project=self.project, key=True)]


def get_issue_file_path(instance, filename):
    return 'uploads/issues_files/%s/%s' % (instance.issue_id if instance.issue_id else datetime.now(), filename)


class File(models.Model):
    issue = models.ForeignKey(Issue, verbose_name=_("Задача"),
                              related_name='files', on_delete=models.CASCADE,
                              null=True, blank=True)
    title = models.CharField(_("Описание файла"), max_length=250, null=True,
                             blank=True)
    file = models.FileField(_("Файл"), upload_to=get_issue_file_path)
    author = models.ForeignKey(Profile, null=True, blank=True,
                               on_delete=models.CASCADE)
    comment = models.ForeignKey('issues.Comment', null=True, blank=True,
                                on_delete=models.CASCADE, related_name='files')
    datetime = models.DateTimeField(_("Время загрузки"), null=True, blank=True,
                                    auto_now_add=True)

    class Meta:
        verbose_name = _("Файл задачи")
        verbose_name_plural = _("Файлы задачи")
        ordering = 'id',

    def filename(self):
        return os.path.basename(self.file.name)

    def filesize(self):
        return "{} KB".format(float(self.file.size) / 1000)


class Block(models.Model):
    issue = models.ForeignKey(Issue, verbose_name=_("Задача"), related_name='blocks', on_delete=models.CASCADE)
    title = models.CharField(_("Название блока"), max_length=250)
    description = models.TextField(_("Описание блока"), null=True, blank=True)
    order = models.IntegerField(_("Порядок вывода"), default=1)

    class Meta:
        verbose_name = _("Блок")
        verbose_name_plural = _("Блоки")
        ordering = 'order',

    def estimate(self):
        return round(sum([i.estimate for i in Item.objects.filter(block=self)]), 2)
    estimate.short_description = _("Оценка")

    def developer_estimate(self):
        return round(sum([i.developer_estimate() for i in Item.objects.filter(block=self)]), 2)
    developer_estimate.short_description = _("+50%")

    def customer_estimate(self):
        return sum([i.customer_estimate() for i in Item.objects.filter(block=self)])
    customer_estimate.short_description = _("Оценка для заказчика")


class Item(models.Model):
    block = models.ForeignKey(Block, verbose_name=_("Блок"), related_name='items', on_delete=models.CASCADE)
    title = models.CharField(_("Пункт"), max_length=250)
    estimate = models.FloatField(_("Оценка"), help_text=_("чч."))
    order = models.IntegerField(_("Порядок вывода"), default=1)
    comment = models.TextField(_("Комментарий"), null=True, blank=True)

    objects = Manager()

    class Meta:
        verbose_name = _("Пункт оценки")
        verbose_name_plural = _("Пункты оценки")
        ordering = 'order',

    def developer_estimate(self):
        """
        [Оценка (будет оплачено)] = [Оценка (расчёт)]х[Проект.Коэффициент проекта]х[Разработчик.Коэффициент разработчика]х(100%+50%)
        """
        if not self.block.issue.developer:
            return 0.0

        res = round(
            self.estimate *
            self.block.issue.project.project_coefficient *
            self.block.issue.developer.developer_coefficient *
            settings.CORRECTIONS_COEFFICIENT,
            2)
        # print('{} * {} * {} * {} = {}'.format(
        #     self.estimate,
        #     self.block.issue.project.project_coefficient,
        #     self.block.issue.developer.developer_coefficient,
        #     settings.CORRECTIONS_COEFFICIENT,
        #     res
        # ))
        return res
    developer_estimate.short_description = _("+50%")

    def customer_estimate(self):
        """
        [Оценка (видит Заказчик)] = [Оценка (будет оплачено)]*[Заказчик.Коэффициент заказчика]
        """
        return round(self.developer_estimate() * self.block.issue.project.customer.customer_coefficient, 2)
    customer_estimate.short_description = _("Оценка для заказчика")


class Comment(models.Model):
    issue = models.ForeignKey(Issue, verbose_name=_("Задача"),
                              related_name='comments', on_delete=models.CASCADE)
    text = models.TextField(_("Текст сообщения"), blank=True)
    author = models.ForeignKey(Profile, verbose_name=_("Автор сообщения"),
                               on_delete=models.CASCADE)
    show_for_customer = models.BooleanField(_("Заказчик увидит сообщение"),
                                            default=False)
    create_dt = models.DateTimeField(_("Время создания"), auto_now_add=True)
    read = models.BooleanField(_("Прочитано"), default=False)
    change_status_comment = models.BooleanField(
        _("Автоматический"), default=False,
        help_text=_("Автоматический коммент при изменении статуса"))
    change_status_comment_text = models.TextField(_("Текст смены статуса"),
                                                  default='', blank=True,
                                                  null=True)

    objects = Manager()

    def html_text(self):
        return markdown.markdown(self.text, extensions=['nl2br'])

    class Meta:
        verbose_name = _("Комментарий")
        verbose_name_plural = _("Комментарии")
        ordering = 'create_dt',

    def __str__(self):
        return '{} {}'.format(self.text, self.change_status_comment_text)
