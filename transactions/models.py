from django.db import models
from django.utils.translation import gettext as _

from accounts.models import User
from customers.models import Customer


class AbstractBaseTransaction(models.Model):

    create_dt = models.DateTimeField(_("Время создания"), auto_now_add=True)
    initial_value = models.FloatField(_("Начальное значение"), default=0.0)
    incoming = models.FloatField(_("Приход"), default=0.0)
    expense = models.FloatField(_("Расход"), default=0.0)
    final_value = models.FloatField(_("Конечное значение"), default=0.0)
    author = models.ForeignKey(User, verbose_name=_("Автор транзакции"), on_delete=models.CASCADE)
    comment = models.CharField(_("Комментарий"), max_length=250)

    class Meta:
        abstract = True

    def get_balance_object_and_field(self):
        raise NotImplementedError

    def save(self, *args, **kwargs):
        obj, filed = self.get_balance_object_and_field()
        self.initial_value = getattr(obj, filed, 0.0)
        self.final_value = self.initial_value + self.incoming - self.expense

        super().save(*args, **kwargs)

        setattr(obj, filed, self.final_value)
        obj.save()

    def __str__(self):
        if self.incoming:
            return "+ %s" % self.incoming
        if self.expense:
            return "- %s" % self.expense
        return ''


class CustomerHourBalanceTransaction(AbstractBaseTransaction):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Транзакция баланса часов заказчика")
        verbose_name_plural = _("Транзакции баланса часов заказчика")

    def get_balance_object_and_field(self):
        return self.customer, 'hour_balance'
