#!/usr/bin/env bash

CURRENT_UID=$(id -u):$(id -g) docker-compose stop
CURRENT_UID=$(id -u):$(id -g) docker-compose up -d testdjangoapp
