from django.contrib import admin

from main.models import StaticPage, MenuItem


@admin.register(StaticPage)
class StaticPageAdmin(admin.ModelAdmin):
    list_display = 'title', 'slug'


@admin.register(MenuItem)
class MenuItemAdmin(admin.ModelAdmin):
    list_display = 'name', 'url', 'order', 'show_top', 'show_bottom', 'show_for_developers', 'show_for_customers', \
                   'show_for_all'
    list_editable = 'url', 'order', 'show_top', 'show_bottom', 'show_for_developers', 'show_for_customers', \
                   'show_for_all'
