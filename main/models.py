from django.db import models
from django.utils.translation import gettext as _
from tinymce.models import HTMLField


class StaticPage(models.Model):
    title = models.CharField(_('Название'), max_length=250)
    slug = models.SlugField(_('Слаг'), unique=True, max_length=250)
    html = HTMLField(_('страница'))

    class Meta:
        verbose_name = _("Статическая страница")
        verbose_name_plural = _("Статические страницы")


class MenuItem(models.Model):
    order = models.IntegerField(_('Порядок'), default=1)
    name = models.CharField(_('Название'), max_length=50)
    url = models.CharField(_('Url'), max_length=100)
    show_top = models.BooleanField(_('Выводить сверху'), default=False)
    show_bottom = models.BooleanField(_('Выводить снизу'), default=False)
    show_for_developers = models.BooleanField(_('Выводить у Разработчиков'), default=False)
    show_for_customers = models.BooleanField(_('Выводить у Заказчиков'), default=False)
    show_for_all = models.BooleanField(_('Выводить у всех'), default=False)

    class Meta:
        verbose_name = _('Пункт меню')
        verbose_name_plural = _('Меню')
        ordering = 'order', 'name'