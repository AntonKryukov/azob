# Generated by Django 2.1.1 on 2019-02-01 17:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MenuItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.IntegerField(default=1, verbose_name='Порядок')),
                ('name', models.CharField(max_length=50, verbose_name='Название')),
                ('url', models.CharField(max_length=100, verbose_name='Url')),
                ('show_top', models.BooleanField(default=False, verbose_name='Выводить сверху')),
                ('show_bottom', models.BooleanField(default=False, verbose_name='Выводить снизу')),
                ('show_for_developers', models.BooleanField(default=False, verbose_name='Выводить у Разработчиков')),
                ('show_for_customers', models.BooleanField(default=False, verbose_name='Выводить у Заказчиков')),
                ('show_for_all', models.BooleanField(default=False, verbose_name='Выводить у всех')),
            ],
            options={
                'verbose_name_plural': 'Меню',
                'verbose_name': 'Пункт меню',
            },
        ),
    ]
