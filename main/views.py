from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.views.csrf import csrf_failure as django_csrf_failure
from mailer import mail_admins

from main.models import StaticPage, MenuItem


def staticpage(request, url):

    if url.startswith('/'):
        url = url[1:]
    if url.endswith('/'):
        url = url[:-1]

    page = get_object_or_404(StaticPage, slug=url)

    return render(request, 'static_page.html', {'page': page})


def menu_context_processor(request):
    from accounts.models import is_user_customer, is_user_developer

    if is_user_customer(request.user):
        menu_qs = MenuItem.objects.filter(Q(show_for_customers=True) | Q(show_for_all=True))
    elif is_user_developer(request.user):
        menu_qs = MenuItem.objects.filter(Q(show_for_developers=True) | Q(show_for_all=True))
    else:
        menu_qs = MenuItem.objects.filter(show_for_all=True)

    return {
        'menu_top': menu_qs.filter(show_top=True),
        'menu_bottom': menu_qs.filter(show_bottom=True)
    }


def csrf_failure(request,  reason=""):

    mail_admins(
        "csrf_failure on {}".format(timezone.now()),
        "{} got hit in the face with Forbidden result\n"
        "path: {}\n"
        "reason: {}\n"
        "csrfmiddlewaretoken={}\n"
        "CSRF_COOKIE in META={}".format(request.user,
                                        request.get_full_path(),
                                        reason,
                                        request.POST.get('csrfmiddlewaretoken'),
                                        request.META.get('CSRF_COOKIE')
                                        )
    )

    return django_csrf_failure(request, reason)
