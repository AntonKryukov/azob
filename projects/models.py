from django.db import models
from django.db.models import Manager
from django.utils.translation import gettext as _

from customers.models import Customer


class Project(models.Model):
    name = models.CharField(_("Название"), max_length=100)
    slug = models.SlugField(_("Слаг"), max_length=100)
    customer = models.ForeignKey(Customer, verbose_name=_("Заказчик"), on_delete=models.CASCADE, related_name='projects')
    project_coefficient = models.FloatField(_("Коэффициент проекта"), default=1.0)
    site = models.CharField(_("Сайт"), max_length=100, null=True, blank=True)
    developers = models.ManyToManyField('accounts.Profile', verbose_name=_("Разработчики"), related_name="projects_for_developers",
                                        through='ProjectDeveloper')

    objects = Manager()

    class Meta:
        verbose_name = _("Проект")
        verbose_name_plural = _("Проекты")

    def __str__(self):
        return self.name

    def default_author(self):
        from accounts.models import Profile
        try:
            return self.customer.staff.filter(type=Profile.TYPE_CUSTOMER)[0]
        except:
            return None

    def responsible_developers(self):
        return [pd.user for pd in
                ProjectDeveloper.objects.filter(project=self, responsible=True)]


class ProjectDeveloper(models.Model):
    user = models.ForeignKey('accounts.Profile', verbose_name=_("Пользователь"),
                             on_delete=models.CASCADE,
                             related_name='develeper_in_projects')
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    key = models.BooleanField(_("Ключевой"), default=False)
    responsible = models.BooleanField(_("Ответственный"), default=False)

    class Meta:
        verbose_name = _("Разработчик")
        verbose_name_plural = _("Разработчики")

    def __str__(self):
        return '{} in {}'.format(self.user.name, self.project)
