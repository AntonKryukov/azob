from django.contrib import admin

from projects.models import Project, ProjectDeveloper


class ProjectDevelopersAdmin(admin.TabularInline):
    model = ProjectDeveloper
    fields = 'user', 'key', 'responsible'
    extra = 0


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = 'name', 'slug', 'customer', 'project_coefficient', 'site'
    search_fields = 'name', 'customer__name', 'site'
    inlines = ProjectDevelopersAdmin,
