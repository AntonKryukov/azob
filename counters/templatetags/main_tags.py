from django import template
from counters.models import Counter

register = template.Library()


@register.simple_tag
def get_counters():
    return Counter.objects.get_all_counters()
