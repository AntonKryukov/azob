from django.contrib import admin

from counters.models import Counter


@admin.register(Counter)
class CounterAdmin(admin.ModelAdmin):
    list_display = ('title', 'order', 'show')
    list_filter = ['show', 'top_code', 'bottom_code']
