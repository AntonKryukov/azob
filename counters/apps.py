from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CountersConfig(AppConfig):
    name = 'counters'
    verbose_name = _('counters')
