from django.conf import settings
from django.core.cache import cache

from django.db import models
from django.utils.translation import ugettext_lazy as _


class CounterManager(models.Manager):

    def get_all_counters(self):
        all_counters = cache.get(settings.COUNTERS_CACHE_KEY)
        if all_counters is None:
            all_counters = list(self.filter(show=True))
            cache.set(settings.COUNTERS_CACHE_KEY, all_counters,
                      timeout=24 * 60 * 60)
        return all_counters


class Counter(models.Model):
    title = models.CharField(_('Название'), unique=True, max_length=100)
    order = models.IntegerField(_('Порядок'), default=0)
    show = models.BooleanField(_('Показывать'), default=True)
    top_code = models.TextField(_('Верхний код'), blank=True, null=True)
    bottom_code = models.TextField(_('Нижний код'), blank=True, null=True)
    header_code = models.TextField(_('Код в заголовке'), blank=True, null=True)

    objects = CounterManager()

    def __str__(self):
        return self.title

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        cache.delete(settings.COUNTERS_CACHE_KEY)
        super().save(force_insert, force_update, using, update_fields)

    def delete(self, using=None, keep_parents=False):
        cache.delete(settings.COUNTERS_CACHE_KEY)
        return super().delete(using, keep_parents)

    class Meta:
        app_label = 'main'
        verbose_name = _('Cчетчик')
        verbose_name_plural = _('Счетчики')
        ordering = ['show', 'order', 'title']
