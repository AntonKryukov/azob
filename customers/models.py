from django.contrib.auth.models import User
from django.db import models
from django.db.models import Manager
from django.utils.translation import gettext as _


class Customer(models.Model):
    company_name = models.CharField(_("Название компании"), max_length=100)
    developers = models.ManyToManyField('accounts.Profile', verbose_name=_("Разработчики"), related_name="developer_in", through='Developer')
    customer_coefficient = models.FloatField(_("Коэффициент заказчика"), default=1.0)
    hour_cost = models.FloatField(_("Стоимость нормо-часа"), default=1500.0)
    site = models.CharField(_("Сайт"), max_length=100, null=True, blank=True)
    hour_balance = models.FloatField(_("Баланс (чч)"), default=0.0, editable=False)

    objects = Manager()

    class Meta:
        verbose_name = _("Заказчик")
        verbose_name_plural = _("Заказчики")

    def __str__(self):
        return self.company_name


class Developer(models.Model):
    user = models.ForeignKey('accounts.Profile', verbose_name=_("Пользователь"), on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    key = models.BooleanField(_("Ключевой"), default=False)
    responsible = models.BooleanField(_("Ответственный"), default=False)

    class Meta:
        verbose_name = _("Разработчик")
        verbose_name_plural = _("Разработчики")

    def __str__(self):
        return self.user.name


