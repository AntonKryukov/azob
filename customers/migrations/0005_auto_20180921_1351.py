# Generated by Django 2.1.1 on 2018-09-21 13:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0004_remove_developer_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='developer',
            name='customer',
        ),
        migrations.DeleteModel(
            name='Developer',
        ),
    ]
