from django.contrib import admin
from django.core.exceptions import ValidationError
from django.forms import ModelForm

from customers.models import Customer, Developer
from transactions.models import CustomerHourBalanceTransaction
from django.utils.translation import gettext as _


class DevelopersAdmin(admin.TabularInline):
    model = Developer
    fields = 'user', 'key', 'responsible'
    extra = 0


class TransactionInlineForm(ModelForm):

    def clean(self):
        if self.has_changed() and self.initial:
            raise ValidationError(
                _("Нельзя редактировать или удалять транзакции. Можно только добавлять"),
                code='Forbidden'
            )
        return super().clean()


class CustomerHourBalanceTransactionAdmin(admin.TabularInline):
    model = CustomerHourBalanceTransaction
    fields = 'create_dt', 'initial_value', 'incoming', 'expense', 'final_value', 'author'
    readonly_fields = 'create_dt', 'initial_value', 'final_value'
    # readonly_fields = 'create_dt',
    ordering = '-create_dt',
    form = TransactionInlineForm
    extra = 0


    # def has_change_permission(self, request, obj=None):
    #     return False
    #
    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = 'company_name', 'customer_coefficient', 'hour_cost', 'hour_balance',
    readonly_fields = 'hour_balance',
    inlines = CustomerHourBalanceTransactionAdmin, DevelopersAdmin,

