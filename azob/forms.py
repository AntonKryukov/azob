from django.forms import Textarea


class EditorFieldWidget(Textarea):
    template_name = 'forms/widgets/textarea.html'