$( document ).ready(function () {


    var floatformat = function(v) {
        return v.toFixed(2).replace('.', ',');
    };

    var round = function (v) {
        return Math.round(v*100)/100;
    };

    var recalcTotal = function() {
        var total_est = 0;
        var total_est_d = 0;
        var total_est_c = 0;
        $('tr.total:visible').each(function (i, el) {
            var est = parseFloat($(el).find('.estimate').text().replace(',', '.'));
            est = est || 0;
            total_est += est;
            var est_d = parseFloat($(el).find('.estimate-developer').text().replace(',', '.'));
            est_d = est_d || 0;
            total_est_d += est_d;
            var est_c = parseFloat($(el).find('.estimate-customer').text().replace(',', '.'));
            est_c = est_c || 0;
            total_est_c += est_c;
        });

        $('.total-estimate').text(floatformat(total_est));
        $('.total-developer-estimate').text(floatformat(total_est_d));
        if (total_est_d == 0) {
            $('.total-developer-estimate').addClass('red')
        } else {
            $('.total-developer-estimate').removeClass('red')
        }
        $('.total-customer-estimate').text(floatformat(total_est_c));
    };

    var initBlocks = function () {

        $(".btn.edit-block").unbind('click');
        $(".btn.edit-block").click(function () {
            var blockId = $(this).data().blockId;
            $(".block-edit-"+blockId).show();
            $(".block-show-"+blockId).hide();
        });

        $('.block-edit>form').unbind('submit');
        $('.block-edit>form').submit(function(e) {
            var blockId = $(this).data().blockId;
            var block = $(this).parents('.block');

            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize()
            }).done(function(data) {
                $(block).html(data);
                var d = $(block).find('.block-show').data();
                $(block).addClass(d.blockClass);
                $('.total-estimate').html(d.issueEstimate);
                $('.total-developer-estimate').html(d.issueDeveloperEstimate);
                if (d.issueDeveloperEstimate == 0) {
                    $('.total-developer-estimate').addClass('red')
                } else {
                    $('.total-developer-estimate').removeClass('red')
                }
                $('.total-customer-estimate').html(d.issueCustomerEstimate);
                initBlocks();
                return false;
            }).fail(function() {
                console.log('fail save block');
            });
            e.preventDefault();
            return false;
          });


        $('.add-item').unbind('click');
        $('.add-item').click(function (e) {
            e.preventDefault();
            var blockId = $(this).data().blockId;
            var count = $(this).data().count;
            var insertBeforeClass = $(this).data().insertBefore;
            var currentCount = parseInt($('.new-items-count-'+blockId).val());

            $.ajax({
                url: addItemUrl,
                data: {
                    'block-id': blockId,
                    'current-count': currentCount,
                    'count': count
                }
            }).done(function (data) {
                $('.'+insertBeforeClass).before(data);
                $('.new-items-count-'+blockId).val(currentCount+count);
                initBlocks();
            });
        });

        $('.delete-item').unbind('click');
        $('.delete-item').click(function () {
            var itemId = $(this).data().itemId;
            var isNewItem = $(this).data().isNewItem;
            if (isNewItem) {
                $('input[name="new-item-delete-' + itemId +'"]').val(1);
            } else {
                $('input[name="item-delete-' + itemId +'"]').val(1);
            }

            $(this).parents('tr.item').hide();
            var block = $(this).parents('.block-edit');
            $(block).find('tr.item:visible .estimate-input').first().blur();
        });

        $('.add-block').unbind('click');
        $('.add-block').click(function () {
            var thisButton = $(this);

            $.ajax({
                url: addBlockUrl,
                data: {}
            }).done(function (data) {
                thisButton.before(data);
                $('.block-edit-0').show();
                initBlocks();
            });

        });

        $('.delete-block').unbind('click');
        $('.delete-block').click(function () {

            if ($(this).data().deleteUrl == false) {
                $(this).parents('.block').find('.estimate-input').each(function (i, e) {
                    $(e).val(0);
                });
                $(this).parents('.block').find('tr.item:visible .estimate-input').first().blur();
                $(this).parents('.block').remove();

            } else {
                $('#confirmDeleteBlockModal .btn-delete').data('deleteUrl', $(this).data().deleteUrl);
                $('#confirmDeleteBlockModal .btn-delete').data('blockClass', $(this).data().blockClass);
            }
        });

        $('.cancel-block-editing').unbind('click');
        $('.cancel-block-editing').click(function () {
            var formId = $(this).data().blockFormId;
            $('#' +formId + ' input[name="cancel-editing"]').val(1);
            $('#'+formId).submit()
        });

        $('.estimate-input').unbind('blur');
        $('.estimate-input').blur(function () {
            // current block
            var block = $(this).parents('.block-edit');
            var block_est = 0;
            var block_est_d = 0;
            var block_est_c = 0;
            $(block).find('tr.item:visible').each(function (i, el) {
                var est = parseFloat($(el).find('.estimate-input').val().replace(',', '.'));
                est = est || 0;
                var est_d = round(est * DK * PK * CorK);
                var est_c = round(est_d * CK);
                block_est += est;
                block_est_d += est_d;
                block_est_c += est_c;
                $(el).find('.estimate-developer').text(floatformat(est_d));
                $(el).find('.estimate-customer').text(floatformat(est_c));
            });
            $(block).find('tr.total .estimate').text(floatformat(block_est));
            $(block).find('tr.total .estimate-developer').text(floatformat(block_est_d));
            $(block).find('tr.total .estimate-customer').text(floatformat(block_est_c));

            recalcTotal();
        });
        recalcTotal();
    };


    $('#confirmDeleteBlockModal .btn-delete').click(function () {
        var deleteUrl = $(this).data().deleteUrl;
        var blockClass = $(this).data().blockClass;
        if (deleteUrl) {
            $.ajax({
                url: deleteUrl
            });
        }
        $('.'+blockClass).hide();
        recalcTotal();
    });

    initBlocks();

});