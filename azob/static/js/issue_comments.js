$( document ).ready(function () {

    var loadComments = function (sort) {
        data = {};
        if (sort) data.order = 1;
        $.ajax({
            url : commentsUrl,
            data: data
        }).done(function (data) {
            $('.discussion').html(data);
            initComments();
            initFileUploads();
        });
    };

    var loadFiles = function () {
        $.ajax({
            url : filesUrl,
        }).done(function (data) {
            $('.files-wrapper').html(data);
        });
    };

    var csrfmiddlewaretokenValue = '';

    var initComments = function () {
        csrfmiddlewaretokenValue = $('.discussion form input[name="csrfmiddlewaretoken"]').val();
        $('.discussion>form').ajaxForm({
            beforeSubmit: function(arr){
                if ($('#file-upload button').hasClass('disabled')) {
                    return false;
                } else {
                    $('#file-upload button').addClass('disabled');
                }
                if (simplemde != undefined) {
                    arr.push({'name': 'text', 'value': simplemde.value()});
                }
            },
            success: function (data) {
                if (data.result == 'refresh') {
                    location.reload();
                }
                $('.discussion').html(data);
                loadFiles();
                initComments();
                initFileUploads();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(textStatus + ': ' + errorThrown);
                alert(jqXHR.responseText);
                $('#file-upload button').removeClass('disabled');
            }
        });


        $('.discussion .comments .sort').click(function () {
            loadComments(true);
        });

        initChangeStatus();
        initMarkdownField();

    };

    loadFiles();
    loadComments(false);

});