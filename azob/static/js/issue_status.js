var initChangeStatus = function() {
    $('.btn-change-status').unbind('click');
    $('.btn-change-status').click(function () {

        $.ajax({
            url: statusUrl.replace('status-tpl', $(this).data().status)
        }).done(function (data) {
            if (data.result == 'refresh') {
                location.reload()
            }
            if (data.result == 'alert') {

                $('#statusAlert .modal-body').text(data.text);
                $('#statusAlert .modal-title').text('Обновите страницу');
                $('#statusAlert').modal('show');
                $('#statusAlert').on('hidden.bs.modal', function (e) {
                    location.reload()
                });
            }

        })

    });
};

$( document ).ready(function () {

    initChangeStatus();

});