$( document ).ready(function () {

    simplemde = undefined;

    var toolbar;
    if (addIssue) {
        toolbar = ["bold", "italic", "heading", "|", "quote", "|", "preview"]
    } else {
        toolbar = ["bold", "italic", "|", "quote", "|", "preview"]
    }

    initMarkdownField = function() {
        var el = $("textarea.markdown")[0];
        if (el != undefined) {
            simplemde = new SimpleMDE({
                element: el,
                toolbar: toolbar
            });
        }
    };

    initMarkdownField();
});