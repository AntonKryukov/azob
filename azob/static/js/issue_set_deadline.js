var initSetDeadline = function() {

    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

    $('#dedline-datepicker').datepicker({
        uiLibrary: 'bootstrap4',
        locale: 'ru-ru',
        format: 'dd.mm.yyyy',
        minDate: today
    });

    $('.btn-set-deadline').click(function () {

        var date = $('#dedline-datepicker').val();
        $.ajax({
            url: setDeadlineUrl,
            data: {date: date}
        }).done(function (data) {
            if (data.result == 'refresh') {
                location.reload()
            }
        })

    });
};

$( document ).ready(function () {

    initSetDeadline();

});