var ininDeleteAttachedFile = function() {
        $('.delete-attached-file').unbind('click');
        $('.delete-attached-file').click(function () {
            var el = $(this).parent();
            $.ajax({
                url: $(this).attr('href'),
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                type: 'get',
                success: function (response) {
                    $(el).remove();
                },
                error: function (response) {

                }
            });
            return false;
        });
    };


    var clearFileInputField = function (id) {
        var control = $("#"+id);
        control.replaceWith( control = control.clone( true ) );
        $("#"+id).val("");
    };


    var initFileUploads = function() {
        $('#multiFiles').on('change', function () {
            $('.loader').show();
            var form_data = new FormData();
            var ins = document.getElementById('multiFiles').files.length;
            for (var x = 0; x < ins; x++) {
                form_data.append("files[]", document.getElementById('multiFiles').files[x]);
            }
            var form = $(this).closest('form');
            var csrf = $(form).find('input[name="csrfmiddlewaretoken"]').val();
            form_data.append('csrfmiddlewaretoken', csrf);
            $.ajax({
                url: uploadUrl,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    $('#file-upload').append(response);
                    ininDeleteAttachedFile();
                    $('.loader').hide();
                    clearFileInputField('multiFiles');
                },
                error: function (response, textStatus, errorThrown) {
                    alert(textStatus + ': '+ errorThrown);
                    $('.loader').hide();
                    clearFileInputField('multiFiles');
                }
            });
            return false;
        });
    };

$( document ).ready(function () {

    initFileUploads();
});