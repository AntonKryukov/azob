from django.db import models

from tinymce.models import HTMLField


class Inclusion(models.Model):
    # site = models.ForeignKey(Site)
    number = models.IntegerField(u'Номер в шаблоне', unique=True)
    name = models.CharField(u'Название', default='', max_length=100)
    html = HTMLField(u'Код вставки', blank=True, default='&nbsp;')

    objects = models.Manager()
    # on_site = CurrentSiteManager()

    class Meta:
        # unique_together = 'site', 'number'
        verbose_name = u"Вставка"
        verbose_name_plural = u"Вставки"

    def __str__(self):
        return '{} {}'.format(self.number, self.name)


class LandingSettings(models.Model):
    use = models.BooleanField(default=False,
                              verbose_name="Использовать эти настройки?")
    users_count = models.IntegerField("Количество пользователей", default=127)

    class Meta:
        verbose_name = 'Настройки лендинга'
        verbose_name_plural = 'Настройки лендинга'

    def __str__(self):
        return 'Настройки лендинга'


def get_landing_settings():
    try:
        return LandingSettings.objects.filter(use=True)[0]
    except IndexError:
        return None
