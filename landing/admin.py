from django.contrib import admin

from landing.models import Inclusion, LandingSettings


class InclusionAdmin(admin.ModelAdmin):
    list_display = 'number', 'name'


admin.site.register(Inclusion, InclusionAdmin)


class LandingSettingsAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Основные данные', {
            'fields': ('use', 'users_count'),
        }),
    )


admin.site.register(LandingSettings, LandingSettingsAdmin)
