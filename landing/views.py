from django.views.generic import TemplateView

from accounts.models import Profile
from customers.models import Customer
from issues.models import Issue
from landing.models import Inclusion, get_landing_settings


class LandingView(TemplateView):
    template_name = 'landing.html'

    def get_context_data(self, **kwargs):
        context = super(LandingView, self).get_context_data(**kwargs)
        landing_settings = get_landing_settings()
        context.update({
            'inclusions': dict((i.number, i) for i in Inclusion.objects.all()),
            'users_count': landing_settings.users_count + Profile.objects.filter(
                type=Profile.TYPE_DEVELOPER).count(),
            'customers_count': Customer.objects.latest('id').id,
            'issues_count': Issue.objects.latest('id').id,
        })
        return context
