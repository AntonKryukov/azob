# Generated by Django 2.1.1 on 2019-03-19 03:31

from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Inclusion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField(unique=True, verbose_name='Номер в шаблоне')),
                ('name', models.CharField(default='', help_text='Необязательно', max_length=100, verbose_name='Название')),
                ('html', tinymce.models.HTMLField(blank=True, default='&nbsp;', verbose_name='Код вставки')),
            ],
            options={
                'verbose_name': 'Вставка',
                'verbose_name_plural': 'Вставки',
            },
        ),
    ]
