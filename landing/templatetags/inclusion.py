from django import template
from django.urls import reverse

register = template.Library()


@register.inclusion_tag('inclusion.html')
def inclusion(inc, user, show_link=True):

    try:
        link = reverse('admin:landing_inclusion_change', args=(inc.id,))
        context = {'html': inc.html, 'user': user, 'link': link,
                   'name': inc.name, 'show_link': show_link}
    except AttributeError:
        context = {}
    return context
