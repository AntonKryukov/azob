from django.urls import path
from django.views.generic import TemplateView

from accounts.views import LoginView, LogoutView, ProfileView, \
    ChangePasswordView, RecoverPasswordView, RecoveringPasswordView, \
    RegistrationView

app_name = 'accounts'
urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path(r'logout/', LogoutView.as_view(), name='logout'),
    path(r'profile/', ProfileView.as_view(), name='profile'),
    path(r'password/change/', ChangePasswordView.as_view(),
         name='change-password'),
    path(r'recover-password/', RecoverPasswordView.as_view(),
         name='recover_password'),
    path(r'recover-password-success/',
         TemplateView.as_view(
             template_name='accounts/recover_password_success.html'),
         name='recover_password_success'),
    path(r'recovering-password/<key>/',
         RecoveringPasswordView.as_view(), name='recovering_password'),
    path(r'recovering-password-success/',
         TemplateView.as_view(
             template_name='accounts/recovering_password_success.html'),
         name='recovering_password_success'),
    path(r'registration/', RegistrationView.as_view(), name='registration'),
    path(r'registration-success/c/',
         TemplateView.as_view(
             template_name='accounts/registratoin_success.html'),
         name='customer_registration_success'),
    path(r'registration-success/d/',
         TemplateView.as_view(
             template_name='accounts/registratoin_success.html'),
         name='developer_registration_success'),
]