from django.contrib import admin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _
from sorl.thumbnail.admin import AdminImageMixin

from accounts.models import Profile

from django.contrib.auth.admin import UserAdmin as AuthUserAdmin


class UserProfileInline(AdminImageMixin, admin.StackedInline):
    model = Profile
    max_num = 1
    can_delete = False

    def has_add_permission(self, request, obj):
        return False


class EmailRequiredMixin(object):
    def __init__(self, *args, **kwargs):
        super(EmailRequiredMixin, self).__init__(*args, **kwargs)
        self.fields['email'].required = True


class MyUserCreationForm(EmailRequiredMixin, UserCreationForm):
    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email__iexact=email):
            raise ValidationError(_('Пользователь с таким адресом электронной '
                                    'почты уже существует'))
        return email


class MyUserChangeForm(EmailRequiredMixin, UserChangeForm):
    pass


class UserAdmin(AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = ((None, {
        'fields': ('email', 'password1', 'password2'),
        'classes': ('wide',)
    }),)

    list_display = 'date_joined', 'profile_name', 'email', 'is_staff', 'profile_type', 'moderated'
    inlines = [UserProfileInline]
    list_filter = 'profile__type', 'profile__moderated', \
                  'is_staff', 'is_superuser', 'is_active', 'groups'
    ordering = '-date_joined',

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(UserAdmin, self).get_inline_instances(request, obj)

    def profile_type(self, obj):
        return obj.profile.get_type_display()
    profile_type.short_description = _('Тип')

    def profile_name(self, obj):
        return obj.profile.name
    profile_name.short_description = _('Имя')

    def moderated(self, obj):
        return mark_safe(
            '<img src="/static/admin/img/icon-{}.svg" alt="False">'.
                format('yes' if obj.profile.moderated else 'no'))
    moderated.short_description = _('Прошел модерацию')


    def save_model(self, request, obj, form, change):
        obj.username = obj.username or obj.email
        super(UserAdmin, self).save_model(request, obj, form, change)


# unregister old user admin
admin.site.unregister(User)
# register new user admin
admin.site.register(User, UserAdmin)
