from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext as _
from django.views.generic import FormView, TemplateView, UpdateView
from mailer import send_html_mail

from accounts.forms import *
from accounts.models import ResetPasswordRequest


class LoginView(FormView):
    form_class = LoginForm
    template_name = 'accounts/login.html'

    def get_success_url(self):
        return self.request.GET.get('next') or (
            None if self.request.is_ajax() else '/')

    def get_template_names(self):
        return 'accounts/login_ajax.html' if self.request.is_ajax() \
            else 'accounts/login.html'

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.request = self.request
        return form


class LogoutView(TemplateView):
    template_name = 'accounts/logout.html'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super().get(request, *args, **kwargs)


@method_decorator(login_required, name='dispatch')
class ProfileView(UpdateView):
    form_class = ProfileForm
    template_name = 'accounts/profile.html'
    success_url = reverse_lazy('accounts:profile')

    def get_object(self, queryset=None):
        return self.request.user.profile

    def get_initial(self):
        initial = super().get_initial()
        initial.update({'email': self.request.user.email})
        return initial


@method_decorator(login_required, name='dispatch')
class ChangePasswordView(FormView):
    form_class = ChangePasswordForm
    template_name = 'accounts/change_password.html'
    success_url = reverse_lazy('accounts:profile')

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        # передадим request в форму чтобы в валидации проверить, правильность
        # пароля текущего пользователя
        form.request = self.request
        return form

    def form_valid(self, form):
        user = self.request.user
        user.set_password(form.cleaned_data['password_new'])
        user.save()
        login(self.request, user)
        return redirect(self.get_success_url())


class RecoverPasswordView(FormView):
    template_name = 'accounts/recover_password.html'
    form_class = RecoverPasswordForm

    def get_success_url(self):
        return reverse('accounts:recover_password_success')

    def form_valid(self, form):
        user = form.cleaned_data.get('username')
        key = ResetPasswordRequest.generate_key(user)
        url = reverse('accounts:recovering_password', args=[key])

        ctx_dict = {
            'url': 'http://{}{}'.format(settings.DOMAIN, url),
            'user': user,
        }
        send_html_mail(_('Сброс пароля на Azob.ru'),
                       render_to_string('accounts/emails/recover_password.txt',
                                        ctx_dict),
                       render_to_string('accounts/emails/recover_password.html',
                                        ctx_dict),
                       settings.DEFAULT_FROM_EMAIL,
                       [user.email],
                       priority="high")

        return super().form_valid(form)


class RecoveringPasswordView(FormView):
    template_name = 'accounts/recovering_password.html'
    password_request = None
    form_class = RecoveringPasswordForm

    def get_success_url(self):
        return reverse('accounts:recovering_password_success')

    def get_form(self, form_class=None):
        key = self.kwargs.get('key')
        r = ResetPasswordRequest.objects.filter(
            key__iexact=key,
            date__gt=datetime.now() - timedelta(days=1)).first()

        if r is None:
            raise Http404

        self.password_request = r
        return super().get_form(form_class)

    def form_valid(self, form):
        f = super().form_valid(form)
        self.password_request.user.set_password(
            form.cleaned_data.get('password'))
        self.password_request.user.save()
        self.password_request.delete()
        return f


class RegistrationView(FormView):
    template_name = 'accounts/registration.html'
    form_class = RegistrationForm

    def get_success_url(self):
        if self.request.user.profile.type == Profile.TYPE_CUSTOMER:
            return reverse('accounts:customer_registration_success')
        elif self.request.user.profile.type == Profile.TYPE_DEVELOPER:
            return reverse('accounts:developer_registration_success')

    def form_valid(self, form):
        user = User(
            email=form.cleaned_data.get('email'),
            username=form.cleaned_data.get('email'),
            is_active=True,
        )
        user.set_password(form.cleaned_data.get('password'))
        user.save()

        user.profile.name = form.cleaned_data.get('name')
        user.profile.type = form.cleaned_data.get('type')
        user.profile.save()

        login(self.request, user)

        send_html_mail(_('Добро пожаловать на сайт Azob.ru'),
                       render_to_string(
                           'accounts/emails/registration_success.txt'),
                       render_to_string(
                           'accounts/emails/registration_success.html'),
                       settings.DEFAULT_FROM_EMAIL,
                       [user.email],
                       priority="high")

        return super().form_valid(form)
