# Generated by Django 2.1.1 on 2019-03-18 10:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_auto_20190314_1247'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='type',
            field=models.CharField(choices=[('none', '-- Не указан --'), ('developer', 'Разработчик'), ('customer', 'Заказчик')], default='none', max_length=50, verbose_name='Тип'),
        ),
    ]
