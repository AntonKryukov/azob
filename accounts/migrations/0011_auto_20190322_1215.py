# Generated by Django 2.1.1 on 2019-03-22 09:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0010_auto_20190322_1009'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='type',
            field=models.CharField(choices=[('developer', 'Разработчик'), ('customer', 'Заказчик')], default='none', max_length=50, verbose_name='Тип'),
        ),
    ]
