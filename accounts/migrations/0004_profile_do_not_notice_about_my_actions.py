# Generated by Django 2.1.1 on 2019-02-12 04:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_remove_profile_full_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='do_not_notice_about_my_actions',
            field=models.BooleanField(default=False, verbose_name='Не извещать об изменениях, которые я сделал сам'),
        ),
    ]
