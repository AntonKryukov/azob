import hashlib
from datetime import datetime
import random

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext as _
from sorl.thumbnail import ImageField

from customers.models import Customer


class DeveloperTechnology(models.Model):
    name = models.CharField(_("Название технологии"), max_length=50)

    class Meta:
        verbose_name = _("Технология")
        verbose_name_plural = _("Технологии")


def get_user_avatar_path(instance, filename):
    return 'uploads/user_avatars/%s/%s.jpg' % (instance.id, filename)


class Profile(models.Model):
    TYPE_NONE = 'none'
    TYPE_DEVELOPER = 'developer'
    TYPE_CUSTOMER = 'customer'
    TYPE_CUSTOMER_AND_DEVELOPER = 'customer&developer'
    TYPE_CHOICES = (
        # (TYPE_NONE, _("-- Не указан --")),
        (TYPE_DEVELOPER, _("Разработчик")),
        (TYPE_CUSTOMER, _("Заказчик")),
        # (TYPE_CUSTOMER_AND_DEVELOPER, _("Заказчик и разработчик")),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = ImageField(_("Аватар"), upload_to=get_user_avatar_path, null=True, blank=True)
    name = models.CharField(_("Имя"), max_length=50, null=True,
                            help_text=_("Обращение. Как к Вам обращаться."))
    type = models.CharField(_("Тип"), max_length=50, choices=TYPE_CHOICES, default=TYPE_NONE)
    developer_coefficient = models.FloatField(_("Коэффициент разработчика"), default=1.0)
    technologies = models.ManyToManyField(DeveloperTechnology, verbose_name=_("Технологии"), blank=True)
    customer = models.ForeignKey(Customer, verbose_name=_("Заказчик"),
                                 related_name='staff', null=True, blank=True,
                                 on_delete=models.CASCADE)
    do_not_notice_about_my_actions = models.BooleanField(
        _('Не извещать об изменениях, которые я сделал сам'), default=False)
    moderated = models.BooleanField(_('Прошел модерацию'), default=False)

    class Meta:
        verbose_name = _("Пользователь")
        verbose_name_plural = _("Пользователь")

    def __str__(self):
        return "{} {}".format(self.name, "({})".format(self.customer.company_name) if self.customer else '')

    def default_project(self):
        if self.type in [Profile.TYPE_CUSTOMER,
                         Profile.TYPE_CUSTOMER_AND_DEVELOPER]:
            try:
                return self.customer.projects.order_by('pk')[0]
            except:
                return None
        if self.type == Profile.TYPE_DEVELOPER:
            try:
                return self.develeper_in_projects.filter(responsible=True)[0].project
            except:
                None


def is_user_customer(user):
    return user.is_authenticated and user.profile.type in [Profile.TYPE_CUSTOMER, Profile.TYPE_CUSTOMER_AND_DEVELOPER]


def is_user_developer(user):
    return user.is_authenticated and user.profile.type in [Profile.TYPE_DEVELOPER, Profile.TYPE_CUSTOMER_AND_DEVELOPER]


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class ResetPasswordRequest(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    key = models.CharField(max_length=40)
    date = models.DateTimeField(auto_now_add=True)

    @classmethod
    def generate_key(cls, user):
        pc = ResetPasswordRequest()
        pc.user = user
        pc.key = hashlib.sha1((datetime.now().isoformat() + str(
            random.randrange(1, 100000000000000000000)) +
                               user.email).encode('utf-8')).hexdigest()
        pc.save()
        return pc.key
