from django import forms
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.db.models import Q
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _

from accounts.models import Profile


class LoginForm(forms.Form):
    username = forms.CharField(
        label='',
        widget=forms.TextInput(
            attrs={'class': 'form-control',
                   'placeholder': _('Адрес электронной почты')}))
    password = forms.CharField(
        label='',
        widget=forms.PasswordInput(
            attrs={'class': 'form-control',
                   'placeholder': _('Пароль')}))

    def clean_username(self):
        return self.cleaned_data['username'].lower()

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(request=self.request, username=username,
                                password=password)
            if user is not None:
                if user.is_active:
                    login(self.request, user)
                else:
                    raise forms.ValidationError(
                        _('Данный пользователь неактивен'))
            else:
                raise forms.ValidationError(_('Неверные пароль или логин'))
        return self.cleaned_data


class ProfileForm(forms.ModelForm):
    password = forms.CharField(label='',
                               widget=forms.PasswordInput(
                                   {'placeholder': _('Введите свой пароль')}
                               ))
    email = forms.EmailField(label=_('Адрес электронной почты'))

    class Meta:
        model = Profile
        fields = ['name', 'email', 'password',
                  'do_not_notice_about_my_actions']

    def clean_password(self):
        password = self.cleaned_data['password']
        profile = self.instance
        if not profile.user.check_password(password):
            raise forms.ValidationError('Неверный пароль')
        return password

    def clean_email(self):
        email = self.cleaned_data['email']
        user = self.instance.user
        if User.objects.exclude(id=user.id).filter(email=email):
            raise forms.ValidationError(
                'Этот адрес электронной почты уже знят')
        return email

    def save(self, commit=True):
        profile = super().save(commit)
        user = profile.user
        user.email = self.cleaned_data['email']
        user.save()
        return profile


class ChangePasswordForm(forms.Form):
    password_old = forms.CharField(label='',
                                   widget=forms.PasswordInput(
                                       {'placeholder': _('Старый пароль')}
                                   ))
    password_new = forms.CharField(label='',
                                   widget=forms.PasswordInput(
                                       {'placeholder': _('Новый пароль')}
                                   ))
    password_new_1 = forms.CharField(label='',
                                     widget=forms.PasswordInput(
                                       {'placeholder':
                                            _('Подтверждение нового пароля')}
                                   ))

    def clean_password_old(self):
        passw = self.cleaned_data['password_old']
        if not self.request.user.check_password(passw):
            raise forms.ValidationError('Неверный пароль')
        return passw

    def clean(self):
        passw = self.cleaned_data['password_new']
        passw1 = self.cleaned_data['password_new_1']
        if passw != passw1:
            self.add_error('password_new_1', 'Два поля с паролями не совпадают')

        return self.cleaned_data


class RecoverPasswordForm(forms.Form):
    username = forms.CharField(label='',
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control',
                                          'placeholder':
                                              _('Адрес электронной почты')}))

    def clean_username(self):
        username = self.cleaned_data.get('username')
        user = User.objects.filter(
            Q(username=username) | Q(email__iexact=username)).first()

        if user is None:
            raise forms.ValidationError(
                u'Пользователь с таким адресом электронной почты не найден')

        return user


class RecoveringPasswordForm(forms.Form):
    password = forms.CharField(label='',
                               widget=forms.PasswordInput(
                                   attrs={'class': 'form-control',
                                          'placeholder': _('Пароль')}))
    password2 = forms.CharField(label='',
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control',
                                           'placeholder': _('Пароль ещё раз')
                                           }))

    def clean(self):
        pass1 = self.cleaned_data.get('password')
        pass2 = self.cleaned_data.get('password2')

        if pass1 != pass2:
            self.add_error('password2', _('Два поля с паролями не совпадают'))

        return self.cleaned_data


class RegistrationForm(forms.Form):
    type = forms.ChoiceField(
        label=_('Зарегистрироваться как:'),
        widget=forms.RadioSelect({'class': 'profile-type'}),
        choices=(
            (Profile.TYPE_CUSTOMER, _('Заказчик')),
            (Profile.TYPE_DEVELOPER, _('Разработчик')))
    )
    email = forms.EmailField(label='',
                             widget=forms.EmailInput(
                                 {'placeholder': _('Адрес электронной почты')}))
    name = forms.CharField(label='',
                           widget=forms.TextInput(
                               {'placeholder': _('Имя')}))
    password = forms.CharField(label='',
                               widget=forms.PasswordInput(
                                   {'placeholder': _('Пароль')}))
    password2 = forms.CharField(label='',
                                widget=forms.PasswordInput(
                                    {'placeholder': _('Пароль ещё раз')}))
    rules = forms.BooleanField(
        label=mark_safe(_('Я прочитал <a target="_blank" '
                          'href="/terms-and-conditions/">правила '
                          'использования</a> и согласен с ними')),
        required=False)

    def clean_rules(self):
        if not self.cleaned_data['rules']:
            raise forms.ValidationError(_('Необходимо прочитать и принять '
                                          'правила использования'))
        return self.cleaned_data

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email):
            raise forms.ValidationError(
                _('Этот адрес электронной почты уже знят'))
        return email

    def clean(self):
        pass1 = self.cleaned_data.get('password')
        pass2 = self.cleaned_data.get('password2')

        if pass1 != pass2:
            self.add_error('password2', _('Два поля с паролями не совпадают'))

        return self.cleaned_data
