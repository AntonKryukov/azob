from django.contrib.auth.decorators import user_passes_test
from django.urls import reverse_lazy


def moderation_required(func):

    actual_decorator = user_passes_test(
        lambda u: u.is_authenticated and u.profile.moderated,
        login_url=reverse_lazy('on_moderation'),
        redirect_field_name=None
    )
    if func:
        return actual_decorator(func)
    return actual_decorator
